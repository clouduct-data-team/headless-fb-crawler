# headless-fetch-util

This util allows you to fetch comments of group and page feeds (posts, aka), albums and group member list through puppeteer.

## Installation

Run `npm install` or `yarn`.

Also `http` command below comes from `httpie`, you can have it by running `apt install httpie`.

## Run API server

By running `npm run server`, server will launch chrome headless and start listening request at port 3003.

## Usage - login

Get your access token first from this `login` endpoint by specifying `email` and `password` via POST method.

`http --timeout 900 post :3003/auth/login email=mark@facebook.com password=lol`

And you'll have a result like this:

```json
{
  "data": {
    "token": "84ad3b3b-5762-4e4e-a8f5-57148e8f9410"
  }
}
```

Get this token ready and append to your http request header as `Authorization`'s value, otherwise, you might get an error message like `token in authorization header is not valid` in response.

## Usage - getTopFan (for page only)

Get your responses from `getTopFan` endpoint by specifying `pageId` and `limit` (defaults to: 1000) via POST method.

`http --timeout 1800 post :3003/page/getTopFan Authorization:[YOUR_TOKEN] pageId=242305665805605 limit=2`

And the result will like this:

```json
{
  "data": [
    {
      "global_id": "1840455085",
      "name": "Amy Hung",
      "picture": "https://scontent.ftpe7-3.fna.fbcdn.net/v/t1.0-1/p74x74/24068126_10207970197213023_3291279838007660074_n.jpg?_nc_cat=0...<snipped>"
    },
    {
      "global_id": "100006483826203",
      "name": "Wen Chen Li",
      "picture": "https://scontent.ftpe7-3.fna.fbcdn.net/v/t1.0-1/c6.6.74.74/p86x86/1001120_1421837744709006_324005594_n.jpg?_nc_cat=0...<snipped>"
    }
  ]
}
```

## Usage - getMember (for group only)

Get your responses from `getMember` endpoint by specifying `groupId` and `limit` (defaults to: 100) via POST method.

`http --timeout 1800 post :3003/group/getMember Authorization:[YOUR_TOKEN] groupId=548767808612930 limit=2`

And the result will like this:

```json
{
  "data": [
    {
      "date": "2018-04-10T16:00:00.000Z",
      "global_id": "1840455085",
      "name": "Amy Hung",
      "picture": "https://scontent.ftpe7-3.fna.fbcdn.net/v/t1.0-1/p74x74/24068126_10207970197213023_3291279838007660074_n.jpg?_nc_cat=0...<snipped>"
    },
    {
      "date": "2018-04-09T16:00:00.000Z",
      "global_id": "100006483826203",
      "name": "Wen Chen Li",
      "picture": "https://scontent.ftpe7-3.fna.fbcdn.net/v/t1.0-1/c6.6.74.74/p86x86/1001120_1421837744709006_324005594_n.jpg?_nc_cat=0...<snipped>"
    }
  ]
}
```

## Usage - getFeedList (for group and page)

Get your responses from `getFeedList` endpoint by specifying `groupId` (or `pageId`) and `limit` (defaults to: 20) via POST method.

`http --timeout 900 post :3003/group/getFeedList Authorization:[YOUR_TOKEN] groupId=548767808612930 limit=2`

`http --timeout 900 post :3003/page/getFeedList Authorization:[YOUR_TOKEN] pageId=MeetStartup limit=2`

And the result will like this:

```json
{
  "data": [
    {
      "created_time": "2018-04-06T23:53:52.000Z",
      "feed_id": "1055788084577564",
      "from": {
        "id": "1131181636",
        "name": "Cary Yen"
      },
      "message": "今天的daily sepcial是短裙，凱莉想說天氣快就變熱了，一條小短裙既可愛，又清新。\n\n4/7下單價：NTD$1280\n4/11前下單：NTD$1380\n4/11後下單；NTD$1580...\nSee More\n",
      "timestamp": "1523058832",
      "type": "photo",  // photo, video or status
      "photo": "https://...."
    },
    {
      "created_time": "2018-04-06T23:49:47.000Z",
      "feed_id": "1055787064577666",
      "from": {
        "id": "1131181636",
        "name": "Cary Yen"
      },
      "message": "大家有空來看一下凱莉介紹幾款物美價廉的必敗單品。",
      "timestamp": "1523058587",
      "type": "status"
    }
  ]
}
```

## Usage - getAlbumList (for group and page)

Get your responses from `getAlbumList` endpoint by specifying `groupId` (or `pageId`) and `limit` (defaults to: 20) via POST method.

`http --timeout 900 post :3003/group/getAlbumList Authorization:[YOUR_TOKEN] groupId=548767808612930 limit=2`

`http --timeout 900 post :3003/page/getAlbumList Authorization:[YOUR_TOKEN] pageId=MeetStartup limit=2`

And the result will like this:

```json
{
  "data": [
    {
      "album_id": "1032979640191742",
      "count": 14,
      "name": "3月份 “AEO”預購相簿"
    },
    {
      "album_id": "1051000951722944",
      "count": 3,
      "name": "4月份 “UA”預購相簿"
    }
  ]
}
```

## Usage - getPhotoList (for group and page)

Get your responses from `getPhotoList` endpoint by specifying `albumId` and `limit` (defaults to: 20) via POST method.

`http --timeout 900 post :3003/group/getPhotoList Authorization:[YOUR_TOKEN] albumId=1028837087272664 limit=2`

`http --timeout 900 post :3003/page/getPhotoList Authorization:[YOUR_TOKEN] albumId=1120211014721687 limit=2`

And the result will like this:

```json
{
  "data": [
    {
      // "name" value only available in group, no hope in page
      "name": "'3月份 “Coach”預購時間：2/22~3/15\n即時查看系統會員購買進度連結：https://www.scpenny.net/\n\n品名：Coach女大人 Coach&KH聯名限量相機包（粉，淺藍）\n售價：NTD$13,980'",
      "photo_id": "10214243598180485",
      "picture": "https://scontent.ftpe7-3.fna.fbcdn.net/v/t1.0-0/p206x206/...<snipped>"
    },
    {
      // "name" value only available in group, no hope in page
      "name": "'3月份 “Coach”預購時間：2/22~3/15\n即時查看系統會員購買進度連結：https://www.scpenny.net/\n\n品名：Coach女大人 Coach&KH聯名限量相機包（黑，藍）\n售價：NTD$13,980'",
      "photo_id": "10214243599420516",
      "picture": "https://scontent.ftpe7-3.fna.fbcdn.net/v/t1.0-0/p206x206/...<snipped>"
    }
  ]
}
```

## Usage - getComment / getPhotoComment (for group and page)

Get your responses from `getComment` endpoint by specifying `feedId` via POST method.

`http --timeout 3600 post :3003/group/getComment Authorization:[YOUR_TOKEN] feedId=1002114489944924`

`http --timeout 3600 post :3003/page/getComment Authorization:[YOUR_TOKEN] feedId=1662791530515037`

Or get your responses from `getPhotoComment` endpoint by specifying `photoId` via POST method.

`http --timeout 3600 post :3003/group/getPhotoComment Authorization:[YOUR_TOKEN] photoId=10214368414420813`

`http --timeout 3600 post :3003/page/getPhotoComment Authorization:[YOUR_TOKEN] photoId=1554670297993828`

And the result will like this:

```json
{
  "data": [
    {
      "created_time": "2018-03-14T04:05:40.000Z",
      "tag": {
        "id": "100022391463488",
        "name": "Scpenny Canada"
      },
      "from": {
        "id": "100000239877715",
        "name": "Wenlin Chang"
      },
      "id": "1041512012671838",
      "reply_to": null,
      "message": "Scpenny Canada小幫手麻煩看一下私訊，謝謝!",
      "post_id": "1002114489944924",
      "reaction": {
        "angry": 0,
        "haha": 0,
        "like": 1,
        "love": 0,
        "sad": 0,
        "wow": 0
      },
      "timestamp": "1521000340",
      "attachment": {}
    },
    {
      "created_time": "2018-03-23T15:04:29.000Z",
      "tag": {
        "id": "",
        "name": ""
      },
      "from": {
        "id": "100001241494970",
        "name": "Loo Lin"
      },
      "id": "1047281825428190",
      "reply_to": "1041512012671838",
      "message": "收到",
      "post_id": "1002114489944924",
      "reaction": {
        "angry": 0,
        "haha": 0,
        "like": 4,
        "love": 2,
        "sad": 0,
        "wow": 0
      },
      "timestamp": "1521817469",
      "attachment": {
        "type": "photo",
        "url": "...",
        "target": {
          "id": "2194665443882063",
          "url": "https://www.facebook.com/photo.php?fbid=2194665443882063&set=p.2194665443882063..."
        },
        "media": {
          "image": {
            "height": "210",
            "width": "118",
            "src": "https://scontent.ftpe7-3.fna.fbcdn.net/.../35240535_2194665447215396_5948997560042520576_n.jpg..."
          }
        }
      }
    }
  ]
}
```

## Usage - getReaction / getPhotoReaction (for group and page)

Get your responses from `getReaction` endpoint by specifying `feedId` via POST method.

`http --timeout 3600 post :3003/group/getReaction Authorization:[YOUR_TOKEN] feedId=1002114489944924`

`http --timeout 3600 post :3003/page/getReaction Authorization:[YOUR_TOKEN] feedId=1662791530515037`

Or get your responses from `getPhotoReaction` endpoint by specifying `photoId` via POST method.

`http --timeout 3600 post :3003/group/getPhotoReaction Authorization:[YOUR_TOKEN] photoId=10214368414420813`

`http --timeout 3600 post :3003/page/getPhotoReaction Authorization:[YOUR_TOKEN] photoId=1554670297993828`

And the result (all 6 reactions) listed below:

```json
{
  "data": [
    {
        "id": "517141867",
        "name": "Hank Yang",
        "reaction": "like"
    },
    {
        "id": "514521852",
        "name": "Chris Tai",
        "reaction": "haha"
    },
    {
        "id": "506916625",
        "name": "Chen Baoz",
        "reaction": "sad"
    },
    {
        "id": "502940715",
        "name": "Kerry Chen",
        "reaction": "love"
    },
    {
        "id": "6858310",
        "name": "Carson Yiu",
        "reaction": "wow"
    },
    {
        "id": "3501621",
        "name": "Yu-Jiu Wang",
        "reaction": "angry"
    }
  ]
}
```

## Usage - replyToFeed / replyToPhoto (for group)

Leave comment to post by specifying `feedId` (or `photoId`) via POST method. (L1 comment)

`http --timeout 3600 post :3003/group/replyToFeed Authorization:[YOUR_TOKEN] feedId=950890705065825 message="MY MESSAGE"`

`http --timeout 3600 post :3003/group/replyToPhoto Authorization:[YOUR_TOKEN] photoId=10156053694970590 message="MY MESSAGE"`

And the result should be as below:

```json
{
  "data": {
    "message": "MY MESSAGE",
  }
}
```

## Usage - replyToComment / replyToPhotoComment (for group)

Leave comment to comment by specifying `commentId` via POST method. (L2 comment)

`http --timeout 3600 post :3003/group/replyToComment Authorization:[YOUR_TOKEN] commentId=1012185385603023 message="MY MESSAGE"`

`http --timeout 3600 post :3003/group/replyToPhotoComment Authorization:[YOUR_TOKEN] commentId=1012185385603023 message="MY MESSAGE"`

And the result should be as below:

```json
{
  "data": {
    "message": "MY MESSAGE",
  }
}
```

## Usage - reactToFeed / reactToComment / reactToReplyComment (for group)

Like to feed / comment / reply-comment by specifying `feedId`, `commentId` or `replyCommentId` via POST method.

`http --timeout 3600 post :3003/group/reactToFeed Authorization:[YOUR_TOKEN] feedId=950890705065825 reaction=like`

`http --timeout 3600 post :3003/group/reactToComment Authorization:[YOUR_TOKEN] commentId=1016385488516346 reaction=like`

`http --timeout 3600 post :3003/group/reactToReplyComment Authorization:[YOUR_TOKEN] replyCommentId=1016917381796490 reaction=like`

And the result should be as below:

```json
{
  "data": {
    "reaction": "like",
  }
}
```
